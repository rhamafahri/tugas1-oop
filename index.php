<?php

require 'ape.php';

$sheep = new animal("Shaun");
echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold blooded: " . $sheep->cold_blooded . "<br><br>";

$kodok = new frog("Buduk");
echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold blooded: " . $kodok->cold_blooded . "<br>";
$kodok->jump();
echo "<br><br>";


$monyet = new ape("Kera sakti");
echo "Name: " . $monyet->name . "<br>";
echo "Legs: " . $monyet->legs . "<br>";
echo "Cold blooded: " . $monyet->cold_blooded . "<br>";
$monyet->yell();
echo "<br><br>";

?>